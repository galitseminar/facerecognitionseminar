# Face Recognition Seminar
#### Note
This documentation doesn't cover the training phase which is more complicated.
After training a model using OpenFace I think that there are easier solutions that use DNN and enable training like:
https://github.com/AlfredXiangWu/LightCNN, https://github.com/davidsandberg/facenet
Those implementations are much cleaner and don't require mixing environments (use only python unlike openface that uses python+lua).
Anyway, I did attach the files that I used for training using OpenFace.
You can look at the guide: https://cmusatyalab.github.io/openface/training-new-models/ for more details.

The following files are used for the prediction phase and used to create the representations (which can then be used to calculate the distance between images).

##### Installations
- Anaconda 3
- Lua
- OpenCV
- DLib

##### Files
* **[Compare Face on transformations Notebook](Compare_Face_on_transformations.ipynb)**
Displays the effect of applying both models on high ps and low ps features
    *  **transformations_trained_on_frontal.npy**
The representations for each image in the [Transformations Folder](Data/transformations) based on the [frontal model](Models/Frontal/model_26.t7)
    * **transformations_trained_on_wild.npy**
The representations for each image in the [Transformations Folder](Data/transformations) based on the [wild model](Models/Wild/nn4.small2.v1.t7)

* **[get_representations.py](get_representaions.py)**
Creates `transformations_trained_on_wild/frontal.npy` by going over [Transformations Folder](Data/transformations) and running the `getRep` on each file.
    - **getRep(imgPath)**: Receives an image path and returns the vector representation of the image. It first crops the image by using DLib face detection and alignment algorithm. After that it sends the cropped/aligned image to a file. That file is then read by `openface_server.lua` which runs the chosen model on the image and returns the vector representation.


* Models
    * [Frontal model](Models/Frontal/model_26.t7) - Pretrained model that was trained on all the files in [Frontal Training Images](Data/mixed_frontal_colorferet_ndb_nivl_jpeg_essex)
    *  [Wild model](Models/Wild/nn4.small2.v1.t7) - Pretrained model that can be downloaded from OpenFace website. It was trained on CASIA-WebFace and FaceScrub
