import numpy as np
import argparse
import itertools

parser = argparse.ArgumentParser()

# Input: 2 images with directory, for example: changed_faces/ak_1.jpg changed_faces/ak_s.jpg
parser.add_argument('imgs', type=str, nargs='+', help="Input images to compare (ex: changed_faces/ak_1.jpg changed_faces/ak_s.jpg)")

args = parser.parse_args()

def get_directory(img):
    # Returns the directory of the image (AR / colorferet / cplu ..)
    return img.split("/")[0]

for (img1, img2) in itertools.combinations(args.imgs, 2):

    # The representation is saved in a numpy file
    # There is a numpy file for each directory

    repr_dict_img1 = np.load(get_directory(img1)+'.npy').item()

    repr_dict_img2 = np.load(get_directory(img2)+'.npy').item()

    # The key of the dictionary is the relative path of the image (example of key: colorferet/00062_931230_fb.jpg)
    repr_img1 = repr_dict_img1[img1]
    repr_img2 = repr_dict_img2[img2]
    print(len(repr_img2))
    d = repr_img1 - repr_img2
    print("Comparing {} with {}.".format(img1, img2))
    print(
        "  + Squared l2 distance between representations: {:0.3f}".format(np.dot(d, d)))
