local M = { }

-- http://stackoverflow.com/questions/6380820/get-containing-path-of-lua-file
function script_path()
   local str = debug.getinfo(2, "S").source:sub(2)
   return str:match("(.*/)")
end

function M.parse(arg)

   local cmd = torch.CmdLine()
   cmd:text()
   cmd:text('OpenFace')
   cmd:text()
   cmd:text('Options:')

   ------------ General options --------------------
   cmd:option('-cache',
              paths.concat('/media/lior/LinuxHDD/datasets', 'openface-work-trainfromscratch-frontal'),
              'Directory to cache experiments and data.')
   cmd:option('-save', '', 'Directory to save experiment.')
   cmd:option('-data','/media/lior/LinuxHDD/datasets/frontal_face_database/frontal_colorferet_ndb_nivl_jpeg_essex',--'/media/lior/LinuxHDD/datasets/MSCeleb-cleaned',--'/media/lior/LinuxHDD/datasets/vgg_face_dataset/aligned',
              'Home of dataset. Images separated by identity.')
   cmd:option('-manualSeed', 2, 'Manually set RNG seed')
   cmd:option('-cuda', true, 'Use cuda.')
   cmd:option('-device', 1, 'Cuda device to use.')
   cmd:option('-nGPU',   1,  'Number of GPUs to use by default')
   cmd:option('-cudnn', true, 'Convert the model to cudnn.')
   cmd:option('-cudnn_bench', false, 'Run cudnn to choose fastest option. Increase memory usage')

   ------------- Data options ------------------------
   cmd:option('-nDonkeys', 2, 'number of donkeys to initialize (data loading threads)')

   ------------- Training options --------------------
   cmd:option('-nEpochs', 1000, 'Number of total epochs to run')
   cmd:option('-epochSize',1000, 'Number of batches per epoch')
   cmd:option('-epochNumber', 1, 'Manual epoch number (useful on restarts)')
   -- GPU memory usage depends on peoplePerBatch and imagesPerPerson.
   cmd:option('-peoplePerBatch', 7, 'Number of people to sample in each mini-batch.')
   cmd:option('-imagesPerPerson', 20, 'Number of images to sample per person in each mini-batch.')
   cmd:option('-testing', true, 'Test with the LFW.')
   cmd:option('-testBatchSize', 50, 'Batch size for testing.')
   cmd:option('-lfwDir', '/media/lior/LinuxHDD/datasets/lfw-aligned-mtcnn', 'LFW aligned image directory for testing.')
   cmd:option('-frontalTestDir','/media/lior/LinuxHDD/datasets/frontal_face_database/frontal_test')

   ---------- Model options ----------------------------------
   cmd:option('-retrain', 'none',--'/media/lior/LinuxHDD/datasets/openface-work/2017-05-03_00-12-16/model_18.t7',--'/media/lior/LinuxHDD/datasets/openface-work/2017-05-04_09-27-47/model_12.t7',--'/home/lior/dev/workspace/face_recognition_seminar/openface/models/openface/nn4.small2.v1.t7',
      'provide path to model to retrain with')
   cmd:option('-modelDef', '/home/lior/dev/workspace/face_recognition_seminar/openface/models/openface/nn4.small2.def.lua', 'path to model definiton')
   cmd:option('-imgDim', 96, 'Image dimension. nn2=224, nn4=96')
   cmd:option('-embSize', 128, 'size of embedding from model')
   cmd:option('-alpha', 0.2, 'margin in TripletLoss')
   cmd:text()

   local opt = cmd:parse(arg or {})
   os.execute('mkdir -p ' .. opt.cache)

   if opt.save == '' then
      opt.save = paths.concat(opt.cache, os.date("%Y-%m-%d_%H-%M-%S"))
   end
   os.execute('mkdir -p ' .. opt.save)

   return opt
end

return M
