import time

start = time.time()

import cv2
import os
import numpy as np
from scipy.misc import imsave
from align_dlib import AlignDlib
from torch_neural_net import TorchNeuralNet
np.set_printoptions(precision=2)

frontal = False
fileDir = os.path.dirname(os.path.realpath(__file__))
modelDir = os.path.join(fileDir, 'Models')
dlibModelDir = os.path.join(fileDir, 'dlib')
transformationsDir = os.path.join(fileDir, 'Data','transformations')

if frontal:
    model_path = os.path.join(modelDir,"Frontal","model_26.t7")
    frontal_wild = "frontal"
else:
    model_path = os.path.join(modelDir,"Wild","nn4.small2.v1.t7")
    frontal_wild = "wild"

def getRep(imgPath,imgDim=96, multiple=False):
    start = time.time()
    bgrImg = cv2.imread(imgPath)
    if bgrImg is None:
        raise Exception("Unable to load image: {}".format(imgPath))

    rgbImg = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)

    start = time.time()

    if multiple:
        bbs = align.getAllFaceBoundingBoxes(rgbImg)
    else:
        bb1 = align.getLargestFaceBoundingBox(rgbImg)
        bbs = [bb1]
    if len(bbs) == 0 or (not multiple and bb1 is None):
        raise Exception("Unable to find a face: {}".format(imgPath))

    print("Face detection took {} seconds.".format(time.time() - start))

    reps = []
    for bb in bbs:
        start = time.time()
        alignedFace = align.align(
            imgDim,
            rgbImg,
            bb,
            landmarkIndices=AlignDlib.OUTER_EYES_AND_NOSE)
        if alignedFace is None:
            raise Exception("Unable to align image: {}".format(imgPath))

        #print("Alignment took {} seconds.".format(time.time() - start))
        #print("This bbox is centered at {}, {}".format(bb.center().x, bb.center().y))

        start = time.time()
        rep = net.forward(alignedFace)

        print("Neural network forward pass took {} seconds.".format(
           time.time() - start))
        reps.append((bb.center().x, rep))
    sreps = sorted(reps, key=lambda x: x[0])
    return (sreps,alignedFace)


if __name__ == '__main__':

    align = AlignDlib(os.path.join(dlibModelDir,"shape_predictor_68_face_landmarks.dat"))
    net = TorchNeuralNet(model_path, imgDim=96,cuda=True)

    versions ={
        'ver1':['original','lips','hair','eye_color','eye_shape','brows_shape'],
        'ver2': ['original','brows_shape','eye_shape','eye_color','hair','lips'],
        'ver3': ['original','hair','lips','eye_color','eye_shape','brows_shape'],
        'ver4': ['original','lips','eye_color','eye_shape','brows_shape','hair']
    }
    lowps = ['original','mouth','eyes','face','skin','nose']
    face_rep = {}
    for actor_name in os.listdir(transformationsDir):
        for ps in os.listdir(os.path.join(transformationsDir,actor_name)):

            if 'aligned' in ps:
                continue
            if ps == 'low-ps':
                for i,image in enumerate(sorted(os.listdir(os.path.join(transformationsDir,actor_name,ps)))):
                    img_path = os.path.join(transformationsDir,actor_name,ps,image)
                    aligned_path = os.path.join(transformationsDir,actor_name,ps+"_aligned")

                    if os.path.isfile(img_path):

                        if not os.path.isdir(aligned_path):
                            os.mkdir(aligned_path)
                        try:
                            print("Getting Representation for: {}".format(image))
                            rep,alignedFace = getRep(img_path)
                            face_rep['_'.join([actor_name,ps,lowps[i]])] = rep[0][1]
                            imsave(os.path.join(aligned_path,image), alignedFace)
                        except Exception as ex:
                            print(ex)
            else:
                for version in os.listdir(os.path.join(transformationsDir,actor_name,ps)):
                    if 'aligned' in version:
                        continue
                    for i,image in enumerate(sorted(os.listdir(os.path.join(transformationsDir,actor_name,ps,version)))):
                        print(image)
                        img_path = os.path.join(transformationsDir,actor_name,ps,version,image)
                        aligned_path = os.path.join(transformationsDir,actor_name,ps,version+"_aligned")

                        if os.path.isfile(img_path):

                            if not os.path.isdir(aligned_path):
                                os.mkdir(aligned_path)
                            try:
                                print("Getting Representation for: {}".format(image))
                                rep,alignedFace = getRep(img_path)
                                face_rep['_'.join([actor_name,ps,version,versions[version][i]])] = rep[0][1]
                                imsave(os.path.join(aligned_path,image), alignedFace)
                            except Exception as ex:
                                print(ex)


    print("Saving Representations")
    np.save("transformations_trained_on_{}.npy".format(frontal_wild), face_rep)
